#include <iostream>
#include "Datastructures.h"
#include "GlobalVariable.h"

using namespace std;

itemType userInput(char* print);

int main() {
	StackArray stackArray;
	StackList stackList;
	BinaryTree binaryTree;
	FixTree fixHandler;

	char choice;

	choice = 'k';

	while (choice != 'q') {
		cout << "\nchoice: ";
		cin >> choice;
		cin.ignore();
		choice = tolower(choice);

		switch (choice) {
			/* stack array test */
		case 'a':
			while (true) {
				itemType input;
				input = userInput("\nstack element key: ");
				if (input == (itemType)0) {
					break;
				}
				stackArray.push((itemType)input);
			}
			break;

			/* stack list test */
		case 'b':
			while (true) {
				itemType input;
				input = userInput("\nstack element key: ");
				if (input == (itemType)0) {
					break;
				}
				stackList.push(nullptr, input);
			}
			break;

			/* debug print of stacks*/
		case 'c':
			while (!stackArray.empty()) {
				cout << stackArray.pop() << " ";
			}
			cout << endl << endl; 
			while (!stackList.empty()) {
				cout << stackList.pop().key << " ";
			}
			break;

			/* add elements to binary tree */
		case 'd':
			while (true) {
				itemType input;
				input = userInput("\nnode element key: ");
				if (input == '0') {
					break;
				}
				binaryTree.add((itemType)input);
			}
			break;

			/* remove element from tree */
		case 'e':
			while (true) {
				binaryTree.display();

				itemType input;
				input = userInput("\nnode element key: ");
				if (input == '0') {
					break;
				}
				binaryTree.remove((itemType)input);
			}
			break;  

		case 'f':
			BinaryTree *thisTree;
			thisTree = new BinaryTree;
			itemType input;
			for (int i = 1000; i >= 0; i--) {
				thisTree->add((itemType)i);
			}
			for (int i = 0; i <= 1000; i++) {
				thisTree->add((itemType)i);
			}	
			delete thisTree;
			break;

		case 'p':
			//fixHandler.postfixTree("AB + CD-/");
			//fixHandler.display();
			fixHandler.inToPostFix("( ( ( 4 + 2 ) + ( 3 * 2 ) ) + ( ( 3 + 4 ) * ( 2 * 5 ) ) ) ");
			break;

			/* no test */
		default:
			cout << "\nno valid input";
			break;
		}
	}

	return 0;
}

itemType userInput(char* print) {
	itemType userInput;
	cout << print;
	cin >> userInput;
	cin.ignore();
	return userInput;
}