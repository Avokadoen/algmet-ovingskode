#pragma once
#include "GlobalVariable.h"

class StackArray {
private:
	itemType *stack;
	int count;
public:
	StackArray(int max = 100);
	~StackArray();
	void push(itemType value);
	itemType pop();
	int	empty();
};

// ----------

struct node {
	itemType key;
	struct node *next;
	struct binaryNode* element;
};

struct binaryHeadNode {
	struct binaryNode *next;
};

struct binaryNode {
	itemType key;
	struct binaryNode *left, *right;
};

class StackList{
private:
	struct node *head, *end;

public:
	StackList();
	~StackList();
	void push(binaryNode* element, itemType value);
	node pop();
	int	empty();
};

class BinaryTree {
protected:
	struct binaryHeadNode *head;
	
public:
	BinaryTree();
	~BinaryTree();
	void preorderTraversal();
	virtual void add(itemType value);
	virtual void writeLR(binaryNode* writeMyLR, bool endLine);
	virtual void display();
	void remove(itemType value);
};

class FixTree : public BinaryTree {
public:
	void postfixTree(char* prefix);
	void inToPostFix(char* infix);
};


/*
class Tree {
private:
	node *head, *end;
public:
	Tree();
	~Tree();
	virtual void add(itemType newEle);
	virtual void remove(itemType ele);
	virtual void display();
};

class BinaryTree: protected Tree {
private:
	binaryNode *elements;

public:
	BinaryTree();
	~BinaryTree();
	virtual void add(itemType newEle);
	virtual void remove(itemType ele);
	virtual void display();
};
*/