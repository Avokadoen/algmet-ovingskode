#include "GlobalVariable.h"
#include "Datastructures.h"
#include <iostream>

int level = 0;

/*--------------------STACKARRAY-------------------------------*/
	StackArray::StackArray(int max) {
		stack = new itemType[max];
		count = 0;		
	}

	StackArray::~StackArray() {
		delete[] stack;
	}

	
	void StackArray::push(itemType value) { // value was "v" 
		stack[count++] = value; 
	}

	itemType StackArray::pop() {
		return stack[--count];
	}

	int	StackArray::empty() {
		return !count;
	}


	/*--------------------STACKLIST-------------------------------*/
	StackList::StackList() {
		head = new node; 
		end = new node;

		head->next = end; 
		end->next = end;
	}

	StackList::~StackList() {
		struct node *current;
		current = head;
		while (current != end) {
			head = current; current = current->next; delete head;
		}
		delete end;
	}


	void StackList::push(binaryNode* element, itemType value) { // value was "v" 
		struct node *newElement = new node;
		newElement->key = value;
		newElement->next = head->next;
		newElement->element = element;
		head->next = newElement;
	}

	node StackList::pop() {
		struct node returnElement;
		struct node *firstEle = head->next;
		head->next = firstEle->next;
		returnElement = *firstEle;

		delete firstEle;
		return returnElement;
	}

	int	StackList::empty() {
		return head->next == end;
	}

	/*--------------------STACKBINARYTREE-------------------------------*/
	BinaryTree::BinaryTree() {
		head = new binaryHeadNode;

		head->next = nullptr;
	}

	BinaryTree::~BinaryTree() {
		struct binaryNode *current, *currentHead;
		
		path currentPath;

		while (head->next) {
			current = head->next;
			currentHead = current;
			while (current->left || current->right) {
				currentHead = current;

				if (current->left) {
					currentPath = left;
					current = current->left;
				}
				else if (current->right) {
					currentPath = right;
					current = current->right;
				}
			}
			if (!head->next->left && !head->next->right) {
				head->next = nullptr;
			}
			else if (currentPath == left) {
				currentHead->left = nullptr;
			}
			else {
				currentHead->right = nullptr;
			}

		}
		delete head;
	}


	void BinaryTree::preorderTraversal() {
		binaryNode* current;
	}


	void BinaryTree::add(itemType value) { // value was "v" 
		struct binaryNode *newElement = new binaryNode;
		newElement->key = value;
		newElement->left = nullptr;
		newElement->right = nullptr;

		struct binaryNode *current;

		if (!head->next) {
			head->next = newElement;
		} 
		else {
			current = head->next;

			while (current) {
				if (value < current->key) {
					if (current->left) {
						current = current->left;
					}
					else {
						current->left = newElement;
						current = nullptr;
					}
				}
				else {
					if (current->right) {
						current = current->right;
					}
					else {
						current->right = newElement;
						current = nullptr;
					}
				}
			}
		}
	}
	void BinaryTree::writeLR(binaryNode* writeMyLR, bool endLine) {
		// TODO: bug med level
		
		if (writeMyLR->left && !writeMyLR->right) {
			std::cout << ((endLine) ? '\n' : ' ') << " " << level  << '|' << writeMyLR->left->key <<" ";
			if (endLine == true) { level++; }
			endLine = true;
		}
		else if (writeMyLR->right && !writeMyLR->left) {
			std::cout << ((endLine) ? '\n' : ' ') << " " << writeMyLR->right->key << '|' << level << " ";
			if (endLine == true) { level++; }
			endLine = true;
			
		}
		else if (writeMyLR->left && writeMyLR->right) {
			std::cout << ((endLine) ? '\n' : ' ') << ' ' << level << '|' << writeMyLR->left->key << " " << writeMyLR->right->key << '|' << level << " ";
			if (endLine == true) { level++; }
			endLine = true;	
		}
	
		// Recursion
		if (writeMyLR->left) {
			writeLR(writeMyLR->left, endLine);
			endLine = false;
		}
		if (writeMyLR->right) {
			
			writeLR(writeMyLR->right, endLine);
			endLine = false;
		}
	
	}

	void BinaryTree::display() {
		level = 0;
		bool endLine = true;
		std::cout << " " << head->next->key;
		writeLR(head->next, endLine);
	}

	void BinaryTree::remove(itemType value) {

		struct binaryNode *currentHead;
		struct binaryNode *current;

		bool found = 0;
		path headPath = top;
		

		if (head->next) {

			current = head->next;
			currentHead = current;
			std::cout << std::endl;
			while (current->left || current->right) {
				currentHead = current;
				if (value < current->key && current->left) {
					std::cout << "left	";
					current = current->left;
					headPath = left;
				}
				else if (value >= current->key && current->right) {
					std::cout << "right	";
					current = current->right;
					headPath = right;
				}
				else {
					break;
				}
			}

			if (current->key == value) {
				std::cout << std::endl << "Delete lowest node with value " << value << std::endl;
				if (headPath == left) {
					currentHead->left = nullptr;
				}
				else if (headPath == right) {
					currentHead->right = nullptr;
				}
				else {
					head->next = nullptr;
				}
			}
			else {
				std::cout << "\nNode eksisterer ikke, eller har barn.\n";
			}
		}
		else {
			std::cout << "\nTre er tomt.";
		}
	}


	void FixTree::postfixTree(char* prefix) {

		StackList opstack;
		binaryNode* current;

		int len = strlen(prefix);
		
		for (int i = 0; i < len; i++) {
			if (prefix[i] != ' ') {
				current = new binaryNode; // Lager ny node:
				current->key = prefix[i];
				current->left = current->right = nullptr;

				if (prefix[i] == '+' || prefix[i] == '*' || 
					prefix[i] == '-' || prefix[i] == '/') 
				{ // operator:
					node popElement;
					popElement = opstack.pop();
					current->right = popElement.element;
					popElement = opstack.pop();
					current->left = popElement.element;
				}
				opstack.push(current, 0); // Lagrer bygd node.
			}
		}
		node popElement;
		popElement = opstack.pop();
		
		head->next = popElement.element;
	}

	void FixTree::inToPostFix(char* infix) {
		int infIndex = 0;
		int resIndex = 0;
		char infixTerm[200];
		char result[200];
		StackList opStack;

		int len = strlen(infix);

		strcpy_s(infixTerm, infix);
		infixTerm[len] = '.';
		infixTerm[len +1] = '\0';

		while (infixTerm[infIndex] != '.') {
			if ((infixTerm[infIndex] >= 65 && infixTerm[infIndex] <= 90) ||
				(infixTerm[infIndex] >= '0' && infixTerm[infIndex] <= '9')) {
				result[resIndex++] = infixTerm[infIndex];
			}
			else if (infixTerm[infIndex] != ' ' && infixTerm[infIndex] != ')') {
				opStack.push(nullptr, (itemType)infixTerm[infIndex]);
			}
			else if (infixTerm[infIndex] == ')') {
				node popKey;
				popKey.key = ' ';
				
				popKey = opStack.pop();
				while (popKey.key != '(') {	
					result[resIndex++] = popKey.key;
					popKey = opStack.pop();
				}
			}
			infIndex++;
		}
		if (infixTerm[infIndex] == '.') {
			node popKey;
			while (!opStack.empty()) {
				popKey = opStack.pop();
				result[resIndex++] = popKey.key;
			}
		}

		result[resIndex] = '\0';
		std::cout << std::endl << result;
		//this->postfixTree(result);
		//this->display();
	}